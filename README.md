# Création VLAN WIFI

<a href="https://wikidocs.univ-lorraine.fr/pages/viewpage.action?pageId=120326216#G%C3%A9n%C3%A9ralit%C3%A9setproc%C3%A9dures-Proc%C3%A9dured'acc%C3%A8sauserviceetr%C3%B4lesdechacun" target="_blank" rel="noopener">Documentation</a> pour la création de nouveaux **VLAN** à l'Université de Lorraine. Suivre scrupuleusement la procédure.

Le document ```EQUIPE_TYPE_plan-adressage.ods``` est disponible dans ce projet. La documentation référence de manière explicite la méthode pour remplir les champs. 
Il faut également consulter le document ```Répartition_des_adressesIP.ods``` pour visualiser et choisir le bon réseau en fonction de votre demande.

Ensuite quand vous avez choisi le **subnet**, il faut vérifier dans <a href="https://pmi.infra.univ-lorraine.fr/" target="_blank" rel="noopener">PMI</a> si celui-ci n'est pas déjà utilisé pour un **VLAN** existant et construire le **subnet** en fonction de l'adressage déjà utilisé.

**Par exemple :** Si vous avez un réseau en ```100.75.238.0/23``` et qu'un **subnet** existe de la forme ```100.75.238.0/26``` (.0 à .63) alors le prochain **subnet** sera le ```100.75.238.64/27``` (de .64 à .95) :  

La création d'un **VLAN WIFI** est véhiculée par l'intervention de plusieurs acteurs dans l'ordre suivant :
- Equipe reorg IP (Pierre Magisson et Benoît Marchal)
- Equipe Lothaire (création du vlan - Olivier lacroix, Annick Faucourt)
- Equipe PMI (gestion PMI, @ MAC)
- Equipe WIFI (création du vlan wifi - Stéphane Dugravot)

Faire la demande auprès du service **reorg IP** par messagerie à l'adresse mail suivante :

[reorg IP](mailto:dn-gt-reorg-reseaux@univ-lorraine.fr)

Et en copie saisir les adresses mail suivantes des équipes qui interviendront dans la création de ce VLAN :
- lothaire@univ-lorraine.fr
- pmi-contact@univ-lorraine.fr
- dn-wifi-gest@univ-lorraine.fr

**Remarque :** Si la création concerne un **VLAN** pour un subnet Ethernet filaire alors nous n'avons pas besoin de l'équipe WIFI, il y aura juste que 3 adresses mail à renseigner.

Il faut préciser les choses suivantes :
- Est-ce du court terme, moyen terme ou long terme ? RECHERCHE, PEDAGOGIE ou AUTRE.
- Au niveau du réseau, établir la taille en fonction du nombre de matériel à connecter : /27 (32) ou /24 (254), ...
- Le VLAN sera associé à un **SSID**.
- Vérifier la fréquence possible du matériel :
  * **2,4 Ghz** ou **5 Ghz**
  * **2,4 Ghz** et **5 Ghz** dans le cas ou la carte réseau est en **802.11 a/b/g**
- Le périmètre : **ENSTIB** 



